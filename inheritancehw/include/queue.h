#ifndef QUEUE_H
#define QUEUE_H
#include "List.h"

class Queue: public List
{
    public:
        void enqueue(const string &);
        string dequeue();
        //virtual void print() const;
        //friend ostream& operator<<(ostream &, const Queue &)
        //i was told we didn't have to do these, which is good because << was broken
        //with some of this extra stuff
};

#endif // QUEUE_H
