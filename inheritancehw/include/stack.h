#ifndef STACK_H
#define STACK_H
#include "List.h"

class Stack: public List
{
    public:
        void push(string name);
        string pop();
        //virtual void print() const;
        //friend ostream &operator<<(ostream&, const Stack&);
        //refer to queue
};

#endif // STACK_H
