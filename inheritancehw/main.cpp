/**
Taran Todd
TCES 203
Assignment on Inheritance

This assignment has us entirely create a linkedlist called List,
then has us implement inheritors stack and queue
included below are many test functions for the methods involved
in these objects
*/

#include "queue.h"
#include "stack.h"
//edited tuesday for case sensitivity

using namespace std;


void testListAddFirst(List&);
void testListAddLast(List&);
void testListRemoveFirst(List&);
void testListRemoveLast(List&);
void testListPrint(List&);
void testListIsEmpty(List&);
void testListSize(List&);
void testEnqueue(Queue&);
void testDequeue(Queue&);
void testPush(Stack&);
void testPop(Stack&);

int main()
{
    List ls("First Thing");
    testListAddFirst(ls);
    testListAddLast(ls);
    testListIsEmpty(ls);
    testListPrint(ls);
    testListRemoveFirst(ls);
    testListRemoveLast(ls);
    testListSize(ls);
    cout<<"NOW EMPTYING LIST FOR FURTHER TESTING"<<endl;
    ls.removeLast();
    ls.removeLast();
    ls.removeLast();
    testListRemoveLast(ls);
    testListRemoveFirst(ls);
    testListIsEmpty(ls);
    testListSize(ls);
    cout<<"NOW POPULATING LIST FOR DESTRUCTOR"<<endl;
    ls.addFirst("Cheese");
    ls.addFirst("Pepperoni");
    ls.addFirst("Anchovies");
    ls.print();

    cout<<"NOW TESTING QUEUE AND STACK"<<endl;
    Queue Q;
    Stack S;
    testEnqueue(Q);
    testDequeue(Q);
    testPush(S);
    testPop(S);
    cout<<"WE'RE DONE!!!!"<<endl;

    return 0;
}

//testing the addfirst function to a list with one thing already in it
void testListAddFirst(List& ls)
{
    cout<<"Testing addFirst"<<endl;
    ls.addFirst("Tacos");
    ls.print();
}

//testing adding to the end of the list, it should now have three things as tested in main
void testListAddLast(List& ls)
{
    cout<<"Testing addLast"<<endl;
    ls.addLast("Nachos");
    ls.print();
}

//testing removal, i do this to a full list and an empty list
void testListRemoveFirst(List& ls)
{
    cout<<"Testing removeFirst"<<endl;
    cout<<ls.removeFirst()<<" removed"<<endl;
    ls.print();
}

//testing removal from the end, i print afterwards so you know what was removed
//as well as printing the removed item explicitly from the return
void testListRemoveLast(List& ls)
{
    cout<<"Testing removeLast"<<endl;
    cout<<ls.removeLast()<<" removed"<<endl;
    ls.print();
}

//simple enough, i call the print function after adding a few things
void testListPrint(List& ls)
{
    cout<<"Testing print, should have pizza and sauce added"<<endl
    <<"to whatever the list has as of now"<<endl;
    ls.addLast("Pizza");
    ls.addLast("Sauce");
    ls.print();
}

//self explanatory
void testListIsEmpty(List& ls)
{
    cout<<"Testing isEmpty"<<endl;
    cout<<"The list is empty if this is 1: "<<ls.isEmpty()<<endl;
}

//here I output the size and label it so in cout
void testListSize(List& ls)
{
    cout<<"Testing size"<<endl;
    cout<<"The list is "<<ls.size()<<" units long"<<endl;
}

//here enqueue should add some items, and the dress should be first in line
void testEnqueue(Queue& Q)
{
    cout<<"--Testing Enqueue"<<endl;
    Q.enqueue("Frilly Dress???");
    Q.enqueue("Something else Very Proper");
    Q.print();
}

//here, the dress should be removed first, then the thing, then EMPTY
void testDequeue(Queue& Q)
{
    cout<<"--Testing Dequeue"<<endl;
    cout<<Q.dequeue()<<" removed"<<endl;
    cout<<Q.dequeue()<<" removed"<<endl;
    cout<<Q.dequeue()<<" removed"<<endl;
}

//here, the programmer's guide should be first, then the gourdo's
void testPush(Stack& S)
{
    cout<<"--Testing Push"<<endl;
    S.push("Programmer's guide for Gourdos");
    S.push("Gourdo's guide for programmers");
    S.print();
}

//since gourdo was added last, it should be removed, then programmer's, then EMPTY
void testPop(Stack& S)
{
    cout<<"--Testing Pop"<<endl;
    cout<<S.pop()<<" removed"<<endl;
    cout<<S.pop()<<" removed"<<endl;
    cout<<S.pop()<<" removed"<<endl;
}
