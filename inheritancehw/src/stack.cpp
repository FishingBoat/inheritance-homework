#include "stack.h"
//edited tuesday for case sensitivity

void Stack::push(string name)
{
    addLast(name);
}
string Stack::pop()
{
    string removed;
    try
    {
        removed=removeLast();
        if(removed=="LIST EMPTY")
            throw removed;
    }
    catch(string rem)
    {
        cout<<"Exception: "<<rem<<endl;
        return "Ex";
    }
    return removed;
}
