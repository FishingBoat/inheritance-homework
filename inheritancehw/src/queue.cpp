#include "queue.h"
//edited tuesday for case sensitivity
//adds to end of line with List's addLast
void Queue::enqueue(const string& name)
{
    addLast(name);
}

//removes from first with List's removeFirst
string Queue::dequeue()
{
    string removed;
    try
    {
        removed=removeFirst();
        if(removed=="LIST EMPTY")
            throw removed;
    }
    catch(string rem)
    {
        cout<<"Exception: "<<rem<<endl;
        return "Ex";
    }
    return removed;
}
