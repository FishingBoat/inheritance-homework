#include "List.h"

//default constructor has one blank node to keep track of
    List::List()
    {
        head=new Node("");
        tail=head;
    }
    
    //overload adds one data node to beginning and separates head and tail
    List::List(string data)
    {
        head=new Node(data);
        tail=new Node("");
        tail->setPrev(head);
        head->setNext(tail);
    }
    
    //adds element to end, just before blank node
    //takes extra care around very small lists in case there is no
    //node to point to it
    void List::addLast(string data)
    {

        Node* temp=new Node(data);
        if(isEmpty())
        {
            head=temp;
            head->setNext(tail);
            tail->setPrev(head);
        }
        else
        {
            temp->setPrev(tail->getPrev());
            temp->setNext(tail);
            tail->getPrev()->setNext(temp);
            tail->setPrev(temp);
        }
    }
    
    //adds new node to head, points to previous head node
    void List::addFirst(string data)
    {
        Node* temp=head;
        head=new Node(data);
        head->setNext(temp);
        temp->setPrev(head);
    }
    
    //removes node just before tail's blank node. takes extra care
    //in smaller lists
    string List::removeLast()
    {
        if(this->isEmpty())
        {
            return "LIST EMPTY";
        }
        //this is just in case my final remover, which assumes three nodes, can't act because of less
        else if(size()==1)
        {
            string data=head->getData();
            delete head;
            head=tail;
            return data;
        }
        else
        {
            string data=tail->getPrev()->getData();
            Node* temp=tail->getPrev();
            temp->getPrev()->setNext(tail);
            tail->setPrev(temp->getPrev());
            delete temp;
            return data;
        }
    }
    //a bit easier than removelast, takesout the head node and points head to the next node
    string List::removeFirst()
    {
        if(this->isEmpty())
        {
            return "LIST EMPTY";
        }
        string data=head->getData();
        Node* temp=head->getNext();
        delete head;
        head=temp;
        return data;
    }
    
    //returns 1 if empty, else 0
    bool List::isEmpty() const
    {
        if(head==tail)
        {
            return 1;
        }
        else {return 0;}
    }
    
    //returns an int count after iterating through the list
    int List::size() const
    {
        if(this->isEmpty())
        {
            return 0;
        }
        int counter=0;
        Node* temp=head;
        while(temp!=tail)
        {
            counter++;
            temp=temp->getNext();
        }
        return counter;
    }
    
    //prints out each element in order on a new line
    void List::print() const
    {
        if(this->isEmpty())
        {
            cout<<"LIST EMPTY"<<endl;
        }
        Node* temp=head;
        while(temp!=tail)
        {
            cout<<temp->getData()<<endl;
            temp=temp->getNext();
        }
    }
    /*
    void List::ostream& operator<<(ostream&, const List& that)
    {
        that->print();
    }
    */
    
    //destroys each dynamic element and reorders the list so it doesn't get lost
    List::~List()
    {
        Node* temp=head;
        while(temp!=tail)
        {
            head=temp->getNext();
            delete temp;
            temp=head;
        }
        delete tail;
    }
